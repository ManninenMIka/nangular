import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MangularInformationComponent } from './mangular-core/mangular-information/mangular-information.component';
import { MangularApodComponent } from './mangular-core/mangular-apod/mangular-apod.component';
import { MangularDashboardComponent } from './mangular-core/mangular-dashboard/mangular-dashboard.component';
import { MangularMarsWeatherComponent } from './mangular-core/mangular-mars-weather/mangular-mars-weather.component';
import { MangularMarsRoverComponent } from './mangular-core/mangular-mars-rover/mangular-mars-rover.component';

const routes: Routes = [
  { path: 'dashboard', component: MangularDashboardComponent },
  { path: 'apod', component: MangularApodComponent },
  { path: 'info', component: MangularInformationComponent },
  { path: 'mars-weather', component: MangularMarsWeatherComponent },
  { path: 'mars-rover', component: MangularMarsRoverComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
