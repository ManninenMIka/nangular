import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NasaUrls} from './commonEnums';

@Injectable({
  providedIn: 'root'
})
export class MangularPlanetService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient) { }

    // FYI: APOD = Astronomy Picture of the Day
    getData()  {
      const url = NasaUrls.Apod;
      return this.http.get(url);
    }

    // FYI: EPIC =   Earth Polychromatic Imaging Camera
    getEpicData() {
      const epicUrl = NasaUrls.EPic;
      return this.http.get(epicUrl);
    }
}
