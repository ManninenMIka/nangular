import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MangularNavigationComponent } from './mangular-core/mangular-navigation/mangular-navigation.component';
import { MangularInformationComponent } from './mangular-core/mangular-information/mangular-information.component';
import { MangularApodComponent } from './mangular-core/mangular-apod/mangular-apod.component';
import { MangularDashboardComponent } from './mangular-core/mangular-dashboard/mangular-dashboard.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MangularMarsWeatherComponent } from './mangular-core/mangular-mars-weather/mangular-mars-weather.component';
import { MangularMarsRoverComponent } from './mangular-core/mangular-mars-rover/mangular-mars-rover.component';

@NgModule({
  declarations: [
    AppComponent,
    MangularNavigationComponent,
    MangularInformationComponent,
    MangularApodComponent,
    MangularDashboardComponent,
    MangularMarsWeatherComponent,
    MangularMarsRoverComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatProgressSpinnerModule
  ],
  exports: [
    MatMenuModule,
    MatButtonModule,
    MatProgressSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
