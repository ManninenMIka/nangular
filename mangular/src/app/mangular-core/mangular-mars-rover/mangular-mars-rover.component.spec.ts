import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MangularMarsRoverComponent } from './mangular-mars-rover.component';

describe('MangularMarsRoverComponent', () => {
  let component: MangularMarsRoverComponent;
  let fixture: ComponentFixture<MangularMarsRoverComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MangularMarsRoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MangularMarsRoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
