import { Component, OnInit } from '@angular/core';
import { NavigationItem } from './navigation-item';

@Component({
  selector: 'app-mangular-navigation',
  templateUrl: './mangular-navigation.component.html',
//  styleUrls: ['./mangular-navigation.sass']
})
export class MangularNavigationComponent implements OnInit {

  items = [
    new NavigationItem('Dashboard', '/dashboard'),
    new NavigationItem('Astronomy Picture of the Day', '/apod'),
    new NavigationItem('Mars Weather', '/mars-weather'),
    new NavigationItem('Mars Rover', '/mars-rover'),
    new NavigationItem('Info', '/info')
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
