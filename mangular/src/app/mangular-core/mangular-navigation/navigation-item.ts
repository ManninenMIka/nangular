export class NavigationItem {
    constructor(
        public name: string,
        public route: string
    ) {}
}
