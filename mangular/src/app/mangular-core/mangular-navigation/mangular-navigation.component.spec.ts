import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MangularNavigationComponent } from './mangular-navigation.component';

describe('MangularNavigationComponent', () => {
  let component: MangularNavigationComponent;
  let fixture: ComponentFixture<MangularNavigationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MangularNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MangularNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
