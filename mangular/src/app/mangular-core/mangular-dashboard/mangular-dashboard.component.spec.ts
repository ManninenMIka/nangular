import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MangularDashboardComponent } from './mangular-dashboard.component';

describe('MangularDashboardComponent', () => {
  let component: MangularDashboardComponent;
  let fixture: ComponentFixture<MangularDashboardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MangularDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MangularDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
