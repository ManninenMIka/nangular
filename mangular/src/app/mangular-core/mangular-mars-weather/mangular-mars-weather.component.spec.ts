import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MangularMarsWeatherComponent } from './mangular-mars-weather.component';

describe('MangularMarsWeatherComponent', () => {
  let component: MangularMarsWeatherComponent;
  let fixture: ComponentFixture<MangularMarsWeatherComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MangularMarsWeatherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MangularMarsWeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
