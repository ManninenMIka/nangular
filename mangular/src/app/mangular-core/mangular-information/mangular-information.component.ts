import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mangular-information',
  templateUrl: './mangular-information.component.html',
  styleUrls: ['./mangular-information.component.css']
})
export class MangularInformationComponent implements OnInit {

  linkName = "NASA API"
  linkValue = "https://api.nasa.gov/"

  constructor() { 
  }

  ngOnInit(): void {
    
  }

}
