import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MangularInformationComponent } from './mangular-information.component';

describe('MangularInformationComponent', () => {
  let component: MangularInformationComponent;
  let fixture: ComponentFixture<MangularInformationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MangularInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MangularInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
