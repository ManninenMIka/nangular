import { Component, OnInit } from '@angular/core';
import { MangularPlanetService } from '../../mangular-planet.service';

@Component({
  selector: 'app-mangular-apod',
  templateUrl: './mangular-apod.component.html',
  styleUrls: ['../../app.component.sass']
})
export class MangularApodComponent implements OnInit {
  public epicId;
  public values;

  constructor(
    private mangularPlanetService: MangularPlanetService,
  ) {}

  ngOnInit(): void {
    this.mangularPlanetService.getData().subscribe((data) => {
      this.values = data;
    });
    this.mangularPlanetService.getEpicData().subscribe((epicData) => {
      this.epicId = epicData[0].identifier;
    });
  }
}
