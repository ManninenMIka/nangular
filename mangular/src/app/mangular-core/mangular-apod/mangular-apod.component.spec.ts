import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MangularApodComponent } from './mangular-apod.component';

describe('MangularApodComponent', () => {
  let component: MangularApodComponent;
  let fixture: ComponentFixture<MangularApodComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MangularApodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MangularApodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
