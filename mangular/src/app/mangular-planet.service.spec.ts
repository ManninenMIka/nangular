import { TestBed } from '@angular/core/testing';
import { MangularPlanetService } from './mangular-planet.service';

describe('MangularPlanetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MangularPlanetService = TestBed.inject(MangularPlanetService);
    expect(service).toBeTruthy();
    expect(service.getEpicData).toBeTruthy();
  });
});
